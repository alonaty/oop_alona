#pragma once

#include <fstream>
#include <iostream>
using namespace std;

class File {
	const string file_name;
	ifstream file;
	bool file_status = true;
public:
	File(const string& name);
	// todo - distractor with close
	void read_next_line();
	int parse_position_file();
};

enum Exceptions{CANNOT_OPEN_FILE, };